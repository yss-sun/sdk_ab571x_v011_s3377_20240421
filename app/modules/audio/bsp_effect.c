/*****************************************************************************
 * Copyright (c) 2023 Shenzhen Bluetrum Technology Co.,Ltd. All rights reserved.
 * File      : bsp_effect.c
 * Function  : 音乐音效模块
 * History   :
 * Created by yutao on 2023-12-1.
 *****************************************************************************/
#include "include.h"


#if BT_MUSIC_EFFECT_EN

#define TRACE_EN                0

#if TRACE_EN
#define TRACE(...)              printf(__VA_ARGS__)
#else
#define TRACE(...)
#endif


void load_code_audio_comm(void);
int music_effect_set_state(MUSIC_EFFECT_ALG alg, u8 state);
void alg_user_effect_process(u8 *buf, u32 samples, u32 nch, u32 is_24bit, u32 pcm_info);
void alg_user_effect_init(void);
void music_spatial_audio_start_do(void);
void music_spatial_audio_stop_do(void);
void music_vbass_audio_start_do(void);
void music_dyeq_audio_start_do(void);
void music_dyeq_audio_stop_do(void);
void music_xdrc_audio_start_do(void);
void music_xdrc_audio_stop_do(void);

void vbass_frame_process(u32* buf, u32 samples, u32 ch_idx, u32 in_24bits);
void dynamic_eq_frame_process(u32* buf, u32 samples, u32 ch_idx, u32 in_24bits);
void xdrc_frame_process(u32* buf, u32 samples, u32 ch_idx, u32 in_24bits);

extern const u16 dac_dvol_tbl_db[61];
extern const u8 *dac_dvol_table;


typedef struct {
    //音效状态机
    volatile u16 music_effect_state;
    u16 music_effect_state_set;
    volatile u8 process_flag;
    u8 audio_comm_init_flag;

    //主频
    u8 sys_clk;

    u32 func_process_flag;
} music_effect_t;

music_effect_t music_effect;

//状态位
#define DBB_STA_BIT             BIT(MUSIC_EFFECT_DBB)
#define SPATIAL_AUDIO_STA_BIT   BIT(MUSIC_EFFECT_SPATIAL_AUDIO)
#define VBASS_STA_BIT           BIT(MUSIC_EFFECT_VBASS)
#define DYEQ_STA_BIT            BIT(MUSIC_EFFECT_DYEQ)
#define XDRC_STA_BIT            BIT(MUSIC_EFFECT_XDRC)
#define USER_ALG_STA_BIT        BIT(MUSIC_EFFECT_ALG_USER)
#define DBB_EN(x)               ((x) & (DBB_STA_BIT))
#define SPATIAL_AUDIO_EN(x)     ((x) & (SPATIAL_AUDIO_STA_BIT))
#define VBASS_EN(x)             ((x) & (VBASS_STA_BIT))
#define DYEQ_EN(x)              ((x) & (DYEQ_STA_BIT))
#define XDRC_EN(x)              ((x) & (XDRC_STA_BIT))
#define USER_ALG_EN(x)          ((x) & (USER_ALG_STA_BIT))
//主频设置
#define DBB_SYSCLK_SEL          SYS_24M
#define SPATIAL_AU_SYSCLK_SEL   SYS_100M
#define VBASS_SYSCLK_SEL        SYS_48M
#define DYEQ_SYSCLK_SEL         SYS_60M     //如果开了BT_MUSIC_EFFECT_DYEQ_VBASS_EN，要跑100M
#define XDRC_SYSCLK_SEL         SYS_100M
#define USER_ALG_SYSCLK_SEL     SYS_48M
//process flag bit
#define PROC_FLAG_ALG_REINIT    BIT(0)
#define PROC_FLAG_ALG_DISABLE   BIT(1)



///音效算法处理函数
AT(.com_text.codecs.pcm)
void msc_pcm_effect_process(u8 *buf, u32 samples, u32 nch, u32 is_24bit, u32 pcm_info)
{
    music_effect_t* cb = &music_effect;
    u16 state = cb->music_effect_state;

//    if (pcm_info & BIT(0)) {
//        //first frame
//    }

    if (state) {
        cb->process_flag = 1;
#if BT_MUSIC_EFFECT_SPATIAL_AU_EN
        if (SPATIAL_AUDIO_EN(state)) {
            if (pcm_info & BIT(0)) {
                v3d_clear_cache();
            }
            v3d_frame_process((u32*)buf, samples, nch, is_24bit);
        }
#endif // BT_MUSIC_EFFECT_SPATIAL_AU_EN
#if BT_MUSIC_EFFECT_VBASS_EN
        if (VBASS_EN(state)) {          //只跑单声道
            vbass_frame_process((u32*)buf, samples, sys_cb.tws_left_channel, is_24bit);
        }
#endif // BT_MUSIC_EFFECT_VBASS_EN
#if BT_MUSIC_EFFECT_DYEQ_EN
        if (DYEQ_EN(state)) {           //只跑单声道
            dynamic_eq_frame_process((u32*)buf, samples, sys_cb.tws_left_channel, is_24bit);
        }
#endif // BT_MUSIC_EFFECT_DYEQ_EN
#if BT_MUSIC_EFFECT_XDRC_EN
        if (XDRC_EN(state)) {           //只跑单声道
            xdrc_frame_process((u32*)buf, samples, sys_cb.tws_left_channel, is_24bit);
        }
#endif // BT_MUSIC_EFFECT_XDRC_EN
#if BT_MUSIC_EFFECT_USER_EN
        if (USER_ALG_EN(state)) {       //用户自定义的算法处理
            alg_user_effect_process(buf, samples, nch, is_24bit, pcm_info);
        }
#endif // BT_MUSIC_EFFECT_USER_EN
        cb->process_flag = 0;
    }
}

///音效部分公共接口
//初始化
void music_effect_init(void)
{
    memset(&music_effect, 0, sizeof(music_effect_t));
    music_effect.sys_clk = SYS_24M;
}

//music线程开始播放
void msc_pcm_out_start_callback(u8 codec)
{
    TRACE("msc_pcm_out_start_callback %d\n", codec);

    music_effect_t* cb = &music_effect;

    if ((codec == 2) || (codec == 3)) {         //LHDC LDAC
        cb->audio_comm_init_flag = 0;
        cb->music_effect_state = 0;
        if (cb->music_effect_state_set) {
            cb->func_process_flag |= PROC_FLAG_ALG_DISABLE;     //关掉算法一些硬件有关的处理
        }
    } else if ((codec == 0) || (codec == 1)) {  //AAC SBC
        if (cb->audio_comm_init_flag == 0) {
            load_code_audio_comm();
            cb->audio_comm_init_flag = 1;
        }
        if (cb->music_effect_state_set) {
            cb->func_process_flag |= PROC_FLAG_ALG_REINIT;  //重新开启算法
        }
    }
}

//music线程停止播放
void msc_pcm_out_stop_callback(u8 codec)
{
    TRACE("msc_pcm_out_stop_callback\n");
}

//进入通话
void music_effect_sco_audio_init_do(void)
{
    music_effect_t* cb = &music_effect;
    cb->audio_comm_init_flag = 0;

#if BT_MUSIC_EFFECT_DBB_EN
    if (DBB_EN(cb->music_effect_state_set)) {
        music_effect_set_state(MUSIC_EFFECT_DBB, 0);
        music_dbb_stop();
        music_set_eq_by_num(sys_cb.eq_mode);
    }
#endif // BT_MUSIC_EFFECT_DBB_EN
#if BT_MUSIC_EFFECT_DYEQ_EN
    if (DYEQ_EN(cb->music_effect_state_set)) {
        music_effect_set_state(MUSIC_EFFECT_DYEQ, 0);
        music_dyeq_audio_stop_do();
    }
#endif // BT_MUSIC_EFFECT_DYEQ_EN
#if BT_MUSIC_EFFECT_XDRC_EN
    if (XDRC_EN(cb->music_effect_state_set)) {
        music_effect_set_state(MUSIC_EFFECT_XDRC, 0);
        music_xdrc_audio_stop_do();
    }
#endif // BT_MUSIC_EFFECT_XDRC_EN
}

//退出通话
void music_effect_sco_audio_exit_do(void)
{
#if BT_MUSIC_EFFECT_DBB_EN
    if (DBB_EN(music_effect.music_effect_state_set)) {
        u8 vol_level = dac_dvol_table[sys_cb.vol] + sys_cb.gain_offset;
        u8 bass_level = music_dbb_get_bass_level();
        if (vol_level > 60) {
            vol_level = 60;
        }
        music_dbb_update_param(vol_level, bass_level);
        music_set_eq_by_num(sys_cb.eq_mode);
        music_effect_set_state(MUSIC_EFFECT_DBB, 1);
    }
#endif // BT_MUSIC_EFFECT_DBB_EN
#if BT_MUSIC_EFFECT_DYEQ_EN
    if (DYEQ_EN(music_effect.music_effect_state_set)) {
        music_dyeq_audio_start_do();
        music_effect_set_state(MUSIC_EFFECT_DYEQ, 1);
    }
#endif // BT_MUSIC_EFFECT_DYEQ_EN
#if BT_MUSIC_EFFECT_XDRC_EN
    if (XDRC_EN(music_effect.music_effect_state_set)) {
        music_xdrc_audio_start_do();
        music_effect_set_state(MUSIC_EFFECT_XDRC, 1);
    }
#endif // BT_MUSIC_EFFECT_XDRC_EN
}

//进出休眠
void music_effect_sfunc_sleep_do(u8 enter)
{
    if (enter == 0) {                    //退出休眠
        sys_clk_req(INDEX_KARAOK, music_effect.sys_clk);
    } else {                             //进入休眠

    }
}

void music_effect_alg_reinit(void)
{
#if BT_MUSIC_EFFECT_SPATIAL_AU_EN
    if (SPATIAL_AUDIO_EN(music_effect.music_effect_state_set)) {
        music_spatial_audio_start_do();
        music_effect_set_state(MUSIC_EFFECT_SPATIAL_AUDIO, 1);
    }
#endif // BT_MUSIC_EFFECT_SPATIAL_AU_EN
#if BT_MUSIC_EFFECT_VBASS_EN
    if (VBASS_EN(music_effect.music_effect_state_set)) {
        music_vbass_audio_start_do();
        music_effect_set_state(MUSIC_EFFECT_VBASS, 1);
    }
#endif // BT_MUSIC_EFFECT_VBASS_EN
#if BT_MUSIC_EFFECT_DYEQ_EN
    if (DYEQ_EN(music_effect.music_effect_state_set)) {
        music_dyeq_audio_start_do();
        music_effect_set_state(MUSIC_EFFECT_DYEQ, 1);
    }
#endif // BT_MUSIC_EFFECT_DYEQ_EN
#if BT_MUSIC_EFFECT_XDRC_EN
    if (XDRC_EN(music_effect.music_effect_state_set)) {
        music_xdrc_audio_start_do();
        music_effect_set_state(MUSIC_EFFECT_XDRC, 1);
    }
#endif // BT_MUSIC_EFFECT_XDRC_EN
#if BT_MUSIC_EFFECT_USER_EN
    if (USER_ALG_EN(music_effect.music_effect_state_set)) {
        alg_user_effect_init();
        music_effect_set_state(MUSIC_EFFECT_ALG_USER, 1);
    }
#endif // BT_MUSIC_EFFECT_USER_EN
    TRACE("music_effect_alg_reinit\n");
}

void music_effect_alg_disable(void)
{
#if BT_MUSIC_EFFECT_SPATIAL_AU_EN
    if (SPATIAL_AUDIO_EN(music_effect.music_effect_state_set)) {
        music_spatial_audio_stop_do();
    }
#endif // BT_MUSIC_EFFECT_SPATIAL_AU_EN
#if BT_MUSIC_EFFECT_DYEQ_EN
    if (DYEQ_EN(music_effect.music_effect_state_set)) {
        music_dyeq_audio_stop_do();
    }
#endif // BT_MUSIC_EFFECT_DYEQ_EN
#if BT_MUSIC_EFFECT_XDRC_EN
    if (XDRC_EN(music_effect.music_effect_state_set)) {
        music_xdrc_audio_stop_do();
    }
#endif // BT_MUSIC_EFFECT_XDRC_EN
    TRACE("music_effect_alg_disable\n");
}

//主线程process
AT(.com_text.effect)
void music_effect_func_process(void)
{
    music_effect_t* cb = &music_effect;
    if (cb->func_process_flag) {
        if (cb->func_process_flag & PROC_FLAG_ALG_REINIT) {
            cb->func_process_flag &= ~PROC_FLAG_ALG_REINIT;
            music_effect_alg_reinit();
        }
        if (cb->func_process_flag & PROC_FLAG_ALG_DISABLE) {
            cb->func_process_flag &= ~PROC_FLAG_ALG_DISABLE;
            music_effect_alg_disable();
        }
    }
}

//设置音效状态
int music_effect_set_state(MUSIC_EFFECT_ALG alg, u8 state)
{
    if (alg >= MUSIC_EFFECT_MAX) {
        return -1;
    }

    music_effect_t* cb = &music_effect;
    u8 delay_cnt = 0;
    u8 sys_clk_req_set = SYS_24M;
    u16 new_state = cb->music_effect_state;

    if (state) {
        if (new_state & BIT(alg)) {
            return -2;
        }
        new_state |= BIT(alg);
    } else {
        if ((new_state & BIT(alg)) == 0) {
            return -2;
        }
        new_state &= ~BIT(alg);
    }

    //调整音效的主频设置
    if (new_state) {
        switch (alg) {
#if BT_MUSIC_EFFECT_DBB_EN
        case MUSIC_EFFECT_DBB:
            sys_clk_req_set = DBB_SYSCLK_SEL;
            break;
#endif // BT_MUSIC_EFFECT_DBB_EN

#if BT_MUSIC_EFFECT_SPATIAL_AU_EN
        case MUSIC_EFFECT_SPATIAL_AUDIO:
            sys_clk_req_set = SPATIAL_AU_SYSCLK_SEL;
            break;
#endif // BT_MUSIC_EFFECT_SPATIAL_AU_EN

#if BT_MUSIC_EFFECT_VBASS_EN
        case MUSIC_EFFECT_VBASS:
            sys_clk_req_set = VBASS_SYSCLK_SEL;
            break;
#endif // BT_MUSIC_EFFECT_VBASS_EN

#if BT_MUSIC_EFFECT_DYEQ_EN
        case MUSIC_EFFECT_DYEQ:
            sys_clk_req_set = DYEQ_SYSCLK_SEL;
            break;
#endif // BT_MUSIC_EFFECT_DYEQ_EN

#if BT_MUSIC_EFFECT_XDRC_EN
        case MUSIC_EFFECT_XDRC:
            sys_clk_req_set = XDRC_SYSCLK_SEL;
            break;
#endif // BT_MUSIC_EFFECT_XDRC_EN

#if BT_MUSIC_EFFECT_USER_EN
        case MUSIC_EFFECT_ALG_USER:
            sys_clk_req_set = USER_ALG_SYSCLK_SEL;
            break;
#endif // BT_MUSIC_EFFECT_USER_EN

        default:
            break;
        }
        if (sys_clk_req_set > cb->sys_clk) {
            sys_clk_req(INDEX_KARAOK, sys_clk_req_set);
            cb->sys_clk = sys_clk_req_set;
        }
    }

    //设置状态
    cb->music_effect_state = new_state;

    //释放音效的主频设置
    if (new_state == 0) {
        while (cb->process_flag) {
            WDT_CLR();
            delay_5ms(1);
            delay_cnt++;
            if (!codecs_pcm_is_start()) {   //音乐播放结束就不等了
                break;
            }
            if (delay_cnt >= 200) {
                printf("music_effect_set_state time out!\n");
                return -3;
            }
        }
        sys_clk_free(INDEX_KARAOK);
        cb->sys_clk = SYS_24M;
    }

    TRACE("music_effect_set_state %x\n", new_state);

    return 0;
}

//获取音效是否已经设置打开（不包含因通话、提示音等暂停的状态）
AT(.com_text.effect)
bool music_effect_get_state(MUSIC_EFFECT_ALG alg)
{
    music_effect_t* cb = &music_effect;
    return (bool)((cb->music_effect_state_set & BIT(alg)) > 0);
}

//获取音效实际的状态
AT(.com_text.effect)
bool music_effect_get_state_real(MUSIC_EFFECT_ALG alg)
{
    music_effect_t* cb = &music_effect;
    return (bool)((cb->music_effect_state & BIT(alg)) > 0);
}

///动态低音音效
#if BT_MUSIC_EFFECT_DBB_EN

const u8 dbb_coef_param[453] = {
/* 等级总数：11    	*/
/* 滤波器类型：peak  	*/
/* 中心频率：50    	*/
/* Q值：0.75      	*/
/* 增益：-10, -8, -6, -4, -2, 0, 2, 4, 6, 8, 10,   	*/
	0x02, 0x0b, 0xf6, 0xf8, 0xfa, 0xfc, 0xfe, 0x00, 0x02, 0x04, 0x06, 0x08, 0x0a, 0xcf, 0x37, 0xf5,
	0x07, 0xc5, 0x3e, 0xeb, 0x07, 0x4c, 0x60, 0xe0, 0x0f, 0xb5, 0x9f, 0x1f, 0xf0, 0x6c, 0x89, 0x1f,
	0xf8, 0xbe, 0x88, 0xf7, 0x07, 0xc9, 0x55, 0xec, 0x07, 0x39, 0xc8, 0xe3, 0x0f, 0xc8, 0x37, 0x1c,
	0xf0, 0x7a, 0x21, 0x1c, 0xf8, 0x09, 0xbe, 0xf9, 0x07, 0xda, 0x2a, 0xed, 0x07, 0x90, 0xd2, 0xe6,
	0x0f, 0x71, 0x2d, 0x19, 0xf0, 0x1f, 0x17, 0x19, 0xf8, 0xfc, 0xde, 0xfb, 0x07, 0x98, 0xc0, 0xed,
	0x07, 0x3e, 0x89, 0xe9, 0x0f, 0xc3, 0x76, 0x16, 0xf0, 0x6d, 0x60, 0x16, 0xf8, 0xaa, 0xf2, 0xfd,
	0x07, 0xd7, 0x18, 0xee, 0x07, 0x27, 0xf5, 0xeb, 0x0f, 0xda, 0x0a, 0x14, 0xf0, 0x80, 0xf4, 0x13,
	0xf8, 0x00, 0x00, 0x00, 0x08, 0xa2, 0x34, 0xee, 0x07, 0x46, 0x1e, 0xee, 0x0f, 0xbb, 0xe1, 0x11,
	0xf0, 0x5f, 0xcb, 0x11, 0xf8, 0xdd, 0x0d, 0x02, 0x08, 0x3e, 0x14, 0xee, 0x07, 0xbc, 0x0b, 0xf0,
	0x0f, 0x45, 0xf4, 0x0f, 0xf0, 0xe6, 0xdd, 0x0f, 0xf8, 0x27, 0x23, 0x04, 0x08, 0x28, 0xb7, 0xed,
	0x07, 0xed, 0xc3, 0xf1, 0x0f, 0x14, 0x3c, 0x0e, 0xf0, 0xb2, 0x25, 0x0e, 0xf8, 0xe0, 0x46, 0x06,
	0x08, 0x13, 0x1c, 0xed, 0x07, 0x90, 0x4c, 0xf3, 0x0f, 0x71, 0xb3, 0x0c, 0xf0, 0x0e, 0x9d, 0x0c,
	0xf8, 0x41, 0x80, 0x08, 0x08, 0xe4, 0x40, 0xec, 0x07, 0xbf, 0xaa, 0xf4, 0x0f, 0x42, 0x55, 0x0b,
	0xf0, 0xdd, 0x3e, 0x0b, 0xf8, 0xcc, 0xd6, 0x0a, 0x08, 0xa7, 0x22, 0xeb, 0x07, 0x0c, 0xe3, 0xf5,
	0x0f, 0xf5, 0x1c, 0x0a, 0xf0, 0x8e, 0x06, 0x0a, 0xf8, 0xc3, 0x45, 0xf4, 0x07, 0xd6, 0x6c, 0xe9,
	0x07, 0x37, 0x98, 0xdd, 0x0f, 0xca, 0x67, 0x22, 0xf0, 0x68, 0x4d, 0x22, 0xf8, 0x84, 0xca, 0xf6,
	0x07, 0xeb, 0x9b, 0xea, 0x07, 0x06, 0x4c, 0xe1, 0x0f, 0xfb, 0xb3, 0x1e, 0xf0, 0x92, 0x99, 0x1e,
	0xf8, 0x4f, 0x31, 0xf9, 0x07, 0x57, 0x83, 0xeb, 0x07, 0x38, 0x9a, 0xe4, 0x0f, 0xc9, 0x65, 0x1b,
	0xf0, 0x5b, 0x4b, 0x1b, 0xf8, 0x10, 0x82, 0xfb, 0x07, 0xf0, 0x25, 0xec, 0x07, 0x8d, 0x8d, 0xe7,
	0x0f, 0x74, 0x72, 0x18, 0xf0, 0x01, 0x58, 0x18, 0xf8, 0x74, 0xc4, 0xfd, 0x07, 0xae, 0x85, 0xec,
	0x07, 0xab, 0x2f, 0xea, 0x0f, 0x56, 0xd0, 0x15, 0xf0, 0xdf, 0xb5, 0x15, 0xf8, 0x00, 0x00, 0x00,
	0x08, 0xb0, 0xa3, 0xec, 0x07, 0x35, 0x89, 0xec, 0x0f, 0xcc, 0x76, 0x13, 0xf0, 0x51, 0x5c, 0x13,
	0xf8, 0x2c, 0x3c, 0x02, 0x08, 0x3d, 0x80, 0xec, 0x07, 0xeb, 0xa1, 0xee, 0x0f, 0x16, 0x5e, 0x11,
	0xf0, 0x98, 0x43, 0x11, 0xf8, 0x77, 0x80, 0x04, 0x08, 0xc4, 0x1a, 0xec, 0x07, 0xb9, 0x80, 0xf0,
	0x0f, 0x48, 0x7f, 0x0f, 0xf0, 0xc6, 0x64, 0x0f, 0xf8, 0x81, 0xd4, 0x06, 0x08, 0xd9, 0x71, 0xeb,
	0x07, 0xd5, 0x2b, 0xf2, 0x0f, 0x2c, 0xd4, 0x0d, 0xf0, 0xa8, 0xb9, 0x0d, 0xf8, 0x22, 0x40, 0x09,
	0x08, 0x2e, 0x83, 0xea, 0x07, 0xc9, 0xa8, 0xf3, 0x0f, 0x38, 0x57, 0x0c, 0xf0, 0xb1, 0x3c, 0x0c,
	0xf8, 0x88, 0xcb, 0x0b, 0x08, 0x8d, 0x4b, 0xe9, 0x07, 0x8c, 0xfc, 0xf4, 0x0f, 0x75, 0x03, 0x0b,
	0xf0, 0xec, 0xe8, 0x0a, 0xf8,
};

void music_dbb_eq_index_init(u32* coef_l, u32* coef_r)
{
    dbb_param_cb_t cb;
    cb.dbb_param = dbb_coef_param;
    cb.param_len = sizeof(dbb_coef_param);
    cb.coef_l = (s32*)coef_l;
    cb.coef_r = (s32*)coef_r;
    cb.dac_band_cnt = BT_MUSIC_EFFECT_DBB_BAND_CNT;
    music_dbb_init(&cb);
}

void music_dbb_audio_start(void)
{
    music_effect_t* cb = &music_effect;
    u8 vol_level = dac_dvol_table[sys_cb.vol] + sys_cb.gain_offset;

    if (DBB_EN(cb->music_effect_state_set)) {
        return;
    }

    cb->music_effect_state_set |= DBB_STA_BIT;

    if (vol_level > 60) {
        vol_level = 60;
    }
    music_dbb_update_param(vol_level, BT_MUSIC_EFFECT_DBB_DEF_LEVEL);
    music_set_eq_by_num(sys_cb.eq_mode);
    music_effect_set_state(MUSIC_EFFECT_DBB, 1);

    TRACE("music_dbb_audio_start\n");
}

void music_dbb_audio_stop(void)
{
    music_effect_t* cb = &music_effect;

    if (DBB_EN(cb->music_effect_state_set) == 0) {
        return;
    }

    cb->music_effect_state_set &= ~DBB_STA_BIT;

    music_effect_set_state(MUSIC_EFFECT_DBB, 0);
    music_dbb_stop();
    music_set_eq_by_num(sys_cb.eq_mode);

    TRACE("music_dbb_audio_stop\n");
}

void music_dbb_audio_set_vol_do(u8 vol_level)
{
    music_effect_t* cb = &music_effect;

    if ((DBB_EN(cb->music_effect_state_set) == 0) || (DBB_EN(cb->music_effect_state) == 0)) {
        return;
    }

    u8 bass_level = music_dbb_get_bass_level();
    int res = music_dbb_update_param(vol_level, bass_level);

    //0:不需要change eq，1:先change eq再调音量，2:先调音量再change eq
    if (res == 1) {
        music_set_eq_by_num(sys_cb.eq_mode);
        dac_vol_set(dac_dvol_tbl_db[vol_level]);
    } else if (res == 2) {
        dac_vol_set(dac_dvol_tbl_db[vol_level]);
        music_set_eq_by_num(sys_cb.eq_mode);
    } else {
        dac_vol_set(dac_dvol_tbl_db[vol_level]);
    }
    TRACE("bass_level: %d, vol: -%d dB\n", bass_level, vol_level);
}

void music_dbb_audio_set_bass_level(u8 bass_level)
{
    music_effect_t* cb = &music_effect;

    if ((DBB_EN(cb->music_effect_state_set) == 0) || (DBB_EN(cb->music_effect_state) == 0)) {
        return;
    }

    u8 vol_level = dac_dvol_table[sys_cb.vol] + sys_cb.gain_offset;

    if (vol_level > 60) {
        vol_level = 60;
    }
    music_dbb_update_param(vol_level, bass_level);
    music_set_eq_by_num(sys_cb.eq_mode);
    TRACE("bass_level: %d, vol: -%d dB\n", bass_level, vol_level);
}

#endif // BT_MUSIC_EFFECT_DBB_EN

///空间音效
#if BT_MUSIC_EFFECT_SPATIAL_AU_EN

void music_spatial_audio_start_do(void)
{
    v3d_init();
    music_set_eq_by_res(RES_BUF_EQ_SPATIAL_AUDIO_EQ, RES_LEN_EQ_SPATIAL_AUDIO_EQ);
}

void music_spatial_audio_start(void)
{
    music_effect_t* cb = &music_effect;

    if (SPATIAL_AUDIO_EN(cb->music_effect_state_set)) {
        return;
    }

    cb->music_effect_state_set |= SPATIAL_AUDIO_STA_BIT;

    if (cb->audio_comm_init_flag) {         //已初始化audio comm，直接开启
        music_spatial_audio_start_do();
        music_effect_set_state(MUSIC_EFFECT_SPATIAL_AUDIO, 1);
    }

    TRACE("music_spatial_audio_start\n");
}

void music_spatial_audio_stop_do(void)
{
    music_set_eq_by_num(sys_cb.eq_mode);
}

void music_spatial_audio_stop(void)
{
    music_effect_t* cb = &music_effect;

    if (SPATIAL_AUDIO_EN(cb->music_effect_state_set) == 0) {
        return;
    }

    cb->music_effect_state_set &= ~SPATIAL_AUDIO_STA_BIT;

    music_spatial_audio_stop_do();

    music_effect_set_state(MUSIC_EFFECT_SPATIAL_AUDIO, 0);

    TRACE("music_spatial_audio_stop\n");
}
#endif // BT_MUSIC_EFFECT_SPATIAL_AU_EN

///虚拟低音
#if BT_MUSIC_EFFECT_VBASS_EN

#define VBASS_CAL_INTENSITY(x,bits)     ((s32)(.5+(0.01f * (x))*(((s32)1)<<(bits))))

u8 vbass_cb[232] AT(.music_buff.vbass);

AT(.audio_text.vbass) WEAK
void vbass_frame_process(u32* buf, u32 samples, u32 ch_idx, u32 in_24bits)
{
    int i;
    s16* pcm16 = (s16*)buf;
    s32* pcm32 = (s32*)buf;
    s16 tmp16;
    s32 tmp32;

    if (in_24bits) {        //24bit转成16bit
        if (ch_idx) {
            for (i = 0; i < samples; i++) {
                tmp16 = (s16)(pcm32[2*i+0]>>8);
                tmp32 = vbass_process(vbass_cb, &tmp16);
                pcm32[2*i+0] = (s32)tmp32<<8;
                pcm32[2*i+1] = (s32)tmp32<<8;
            }
        } else {
            for (i = 0; i < samples; i++) {
                tmp16 = (s16)(pcm32[2*i+1]>>8);
                tmp32 = vbass_process(vbass_cb, &tmp16);
                pcm32[2*i+0] = (s32)tmp32<<8;
                pcm32[2*i+1] = (s32)tmp32<<8;
            }
        }
    } else {
        if (ch_idx) {
            for (i = 0; i < samples; i++) {
                tmp16 = pcm16[2*i+0];
                tmp32 = vbass_process(vbass_cb, &tmp16);
                pcm16[2*i+0] = (s16)tmp32;
                pcm16[2*i+1] = (s16)tmp32;
            }
        } else {
            for (i = 0; i < samples; i++) {
                tmp16 = pcm16[2*i+1];
                tmp32 = vbass_process(vbass_cb, &tmp16);
                pcm16[2*i+0] = (s16)tmp32;
                pcm16[2*i+1] = (s16)tmp32;
            }
        }
    }
}

void music_vbass_audio_start_do(void)
{
    vbass_param_cb_t vbass_param_cb;
    vbass_param_cb.cutoff_frequency = 2;
    vbass_param_cb.intensity_set = 50;
    vbass_param_cb.vbass_high_frequency_set = 2;
    vbass_param_cb.pregain = 0x800000;
    vbass_param_cb.intensity = VBASS_CAL_INTENSITY(vbass_param_cb.intensity_set, 15);
    vbass_init(vbass_cb, &vbass_param_cb);
}

void music_vbass_audio_start(void)
{
    music_effect_t* cb = &music_effect;

    if (VBASS_EN(cb->music_effect_state_set)) {
        return;
    }

    cb->music_effect_state_set |= VBASS_STA_BIT;

    if (cb->audio_comm_init_flag) {         //已初始化audio comm，直接开启
        music_vbass_audio_start_do();
        music_effect_set_state(MUSIC_EFFECT_VBASS, 1);
    }

    TRACE("music_vbass_audio_start\n");
}

void music_vbass_audio_stop(void)
{
    music_effect_t* cb = &music_effect;

    if (VBASS_EN(cb->music_effect_state_set) == 0) {
        return;
    }

    cb->music_effect_state_set &= ~VBASS_STA_BIT;

    music_effect_set_state(MUSIC_EFFECT_VBASS, 0);

    TRACE("music_vbass_audio_stop\n");
}

void music_vbass_set_param(u32 cutoff_frequency, u32 intensity, u8 vbass_high_frequency_set, u32 pregain)
{
    vbass_set_param(vbass_cb, cutoff_frequency, intensity, vbass_high_frequency_set, pregain);
    TRACE("music_vbass_set_param %d %d %d\n", cutoff_frequency, intensity, vbass_high_frequency_set);
}
#endif // BT_MUSIC_EFFECT_VBASS_EN

///动态EQ
#if BT_MUSIC_EFFECT_DYEQ_EN

#define VBASS_CAL_INTENSITY(x, bits)    ((s32)(.5+(0.01f * (x))*(((s32)1)<<(bits))))

u8 dyeq_cb[200] AT(.music_buff.dyeq);
u8 dyeq_drc_cb[84] AT(.music_buff.dyeq);
soft_vol_t dyeq_soft_vol AT(.music_buff.dyeq);
#if BT_MUSIC_EFFECT_DYEQ_VBASS_EN
u8 dyeq_vbass_cb[232] AT(.music_buff.vbass);
#endif // BT_MUSIC_EFFECT_DYEQ_VBASS_EN

AT(.audio_rodata.dyeq)
static s32 dyeq_param_tbl[6] = {
    /* 增益：10db */
    /* 阈值1：-20db */
    /* 阈值2：-10db */
    /* 检测启动时间：2ms */
    /* 检测释放时间：300ms */
    0x05000000, 0xfe56cb10, 0xff2b6588, 0x000ccccc, 0x0002e664, 0x00000501,
};

AT(.audio_rodata.dyeq)
static s32 cosw_alpha_tbl_q27[4*5] = {
    /* 频率 : 150Hz */
    /* 低频截止频率 : 20Hz */
    /* 高频截止频率 : 200Hz */
    /* 增益 : 10db */
    /* Q值 : 0.7 */
    /* 滤波器类型 : Peak */
    0x07fc6da5, 0xf00724b8, 0x07fc6da5, 0x0ff8d97e,
    0xf80722ec, 0x0000c4f3, 0x000189e6, 0x0000c4f3,
    0x0f947399, 0xf868789d, 0x0822a66a, 0xf020d53d,
    0x07bd4ce5, 0x0fdf2ac4, 0xf8200cb2, 0x08000000,
    0xf0396d41, 0x07c75a14, 0x0fc692c0, 0xf838a5ed,
};

AT(.audio_text.dyeq)
s32 dyeq_mono_process(s16 data)
{
    s32 tmp32;

    soft_vol_process_mono_one_sample(&dyeq_soft_vol, &data);
#if BT_MUSIC_EFFECT_DYEQ_VBASS_EN
    tmp32 = vbass_process(dyeq_vbass_cb, &data);
#else
    tmp32 = (s32)data;
#endif // BT_MUSIC_EFFECT_DYEQ_VBASS_EN
    dynamic_eq_process(dyeq_cb, &tmp32);
    tmp32 = dyeq_drc_v3_calc(tmp32, dyeq_drc_cb);
    return tmp32;
}

AT(.audio_text.dyeq)
void dynamic_eq_frame_process(u32* buf, u32 samples, u32 ch_idx, u32 in_24bits)
{
    int i;
    s32 tmp32;
    s16 tmp16;
    s32 *pcm32 = (s32*)buf;
    s16 *pcm16 = (s16*)buf;

    if (in_24bits) {
        if (ch_idx == 1) {
            for (i = 0; i < samples; i++) {
                tmp16 = (s16)(pcm32[2*i+0]>>8);
                tmp32 = dyeq_mono_process(tmp16);
                pcm32[2*i+0] = (s32)(tmp32<<8);
                pcm32[2*i+1] = (s32)(tmp32<<8);
            }
        } else {
            for (i = 0; i < samples; i++) {
                tmp16 = (s16)(pcm32[2*i+1]>>8);
                tmp32 = dyeq_mono_process(tmp16);
                pcm32[2*i+1] = (s32)(tmp32<<8);
                pcm32[2*i+0] = (s32)(tmp32<<8);
            }
        }
    } else {
        if (ch_idx == 1) {
            for (i = 0; i < samples; i++) {
                tmp16 = pcm16[2*i+0];
                tmp32 = dyeq_mono_process(tmp16);
                pcm16[2*i+0] = (s16)tmp32;
                pcm16[2*i+1] = (s16)tmp32;
            }
        } else {
            for (i = 0; i < samples; i++) {
                tmp16 = pcm16[2*i+1];
                tmp32 = dyeq_mono_process(tmp16);
                pcm16[2*i+1] = (s16)tmp32;
                pcm16[2*i+0] = (s16)tmp32;
            }
        }
    }
}

void music_dyeq_audio_start_do(void)
{
    u8 vol_level = dac_dvol_table[sys_cb.vol] + sys_cb.gain_offset;
    dyeq_param_cb_t dyeq_param_cb;

    dyeq_param_cb.dyeq_param  = dyeq_param_tbl;
    dyeq_param_cb.coef_param = cosw_alpha_tbl_q27;
    dyeq_param_cb.vbass_en = BT_MUSIC_EFFECT_DYEQ_VBASS_EN;
    dyeq_init(dyeq_cb, &dyeq_param_cb);
    soft_vol_init(&dyeq_soft_vol);
    dyeq_drc_v3_init((u8*)RES_BUF_EQ_DYEQ_DRC, RES_LEN_EQ_DYEQ_DRC, dyeq_drc_cb);

#if BT_MUSIC_EFFECT_DYEQ_VBASS_EN
    vbass_param_cb_t vbass_param_cb;
    vbass_param_cb.cutoff_frequency = 2;
    vbass_param_cb.intensity_set = 50;
    vbass_param_cb.vbass_high_frequency_set = 2;
    vbass_param_cb.pregain = 0x800000;
    vbass_param_cb.intensity = VBASS_CAL_INTENSITY(vbass_param_cb.intensity_set, 15);
    vbass_init(dyeq_vbass_cb, &vbass_param_cb);
#endif // BT_MUSIC_EFFECT_DYEQ_VBASS_EN

    if (vol_level > 60) {
        vol_level = 60;
    }
    dac_vol_set(DIG_N0DB);
    soft_vol_set_vol_param(&dyeq_soft_vol, dac_dvol_tbl_db[vol_level], 1);
}

void music_dyeq_audio_start(void)
{
    music_effect_t* cb = &music_effect;

    if (DYEQ_EN(cb->music_effect_state_set)) {
        return;
    }

    cb->music_effect_state_set |= DYEQ_STA_BIT;

    if (cb->audio_comm_init_flag) {         //已初始化audio comm，直接开启
        music_dyeq_audio_start_do();
        music_effect_set_state(MUSIC_EFFECT_DYEQ, 1);
    }

    TRACE("music_dyeq_audio_start\n");
}

void music_dyeq_audio_stop_do(void)
{
    u8 vol_level = dac_dvol_table[sys_cb.vol] + sys_cb.gain_offset;
    dac_vol_set(dac_dvol_tbl_db[vol_level]);
}

void music_dyeq_audio_stop(void)
{
    music_effect_t* cb = &music_effect;

    if (DYEQ_EN(cb->music_effect_state_set) == 0) {
        return;
    }

    cb->music_effect_state_set &= ~DYEQ_STA_BIT;

    music_dyeq_audio_stop_do();

    music_effect_set_state(MUSIC_EFFECT_DYEQ, 0);

    TRACE("music_dyeq_audio_stop\n");
}

void music_dyeq_set_param(u8 *buf)
{
    dyeq_coef_update(dyeq_cb, buf);
}

void music_dyeq_drc_set_param(void *buf)
{
    dyeq_drc_v3_set_param(buf, dyeq_drc_cb);
}

void music_dyeq_audio_set_vol_do(u8 vol_level)
{
    soft_vol_set_vol_param(&dyeq_soft_vol, dac_dvol_tbl_db[vol_level], 0);
    TRACE("music_dyeq_audio_set_vol_do %d\n", vol_level);
}

#if BT_MUSIC_EFFECT_DYEQ_VBASS_EN
void music_vbass_set_param(u32 cutoff_frequency, u32 intensity, u8 vbass_high_frequency_set, u32 pregain)
{
    vbass_set_param(dyeq_vbass_cb, cutoff_frequency, intensity, vbass_high_frequency_set, pregain);
    TRACE("music_vbass_set_param %d %d %d %d\n", cutoff_frequency, intensity, vbass_high_frequency_set, pregain);
}
#endif // BT_MUSIC_EFFECT_DYEQ_VBASS_EN

#endif // BT_MUSIC_EFFECT_DYEQ_EN

#if BT_MUSIC_EFFECT_XDRC_EN

soft_vol_t xdrc_soft_vol AT(.music_buff.xdrc);

#if BT_MUSIC_EFFECT_XDRC_EQ_EN
s32 xdrc_preeq_cb[BT_MUSIC_EFFECT_XDRC_EQ_BAND*7 + 6] AT(.music_buff.xdrc);  //*coef, *zpara, band, pre_gain, coef(5*BAND), calc_buf((BAND+1)*2)
#endif
#if BT_MUSIC_EFFECT_XDRC_DELAY_CNT
u8  xdrc_delay_cb[16] AT(.music_buff.xdrc);
s32 xdrc_delay_buf[256] AT(.music_buff.xdrc);
#endif

//lp_cb
s32 xdrc_lp_cb[1*7 + 6] AT(.music_buff.xdrc);
s32 xdrc_lp_cb2[1*7 + 6] AT(.music_buff.xdrc);   //分频点过两次
//hp_cb
s32 xdrc_hp_cb[1*7 + 6] AT(.music_buff.xdrc);
s32 xdrc_hp_cb2[1*7 + 6] AT(.music_buff.xdrc);   //分频点过两次
//lp_cb
s32 xdrc_lp_exp_cb[1*7 + 6] AT(.music_buff.xdrc);
//hp_cb
s32 xdrc_hp_exp_cb[1*7 + 6] AT(.music_buff.xdrc);
//drc_cb
u8 xdrc_drclp_cb[14 * 4] AT(.music_buff.xdrc);
//drc_cb
u8 xdrc_drchp_cb[14 * 4] AT(.music_buff.xdrc);
//drc_cb
u8 xdrc_drcall_cb[14 * 4] AT(.music_buff.xdrc);

AT(.text.xdrc)
void xdrc_param_init(void)
{
#if BT_MUSIC_EFFECT_XDRC_DELAY_EN
    pcmdelay_init(xdrc_delay_cb, xdrc_delay_buf,sizeof(xdrc_delay_buf), 4, BT_MUSIC_EFFECT_XDRC_DELAY_CNT);
#endif // BT_MUSIC_EFFECT_XDRC_DELAY_EN

#if BT_MUSIC_EFFECT_XDRC_EQ_EN
    xdrc_softeq_cb_init(xdrc_preeq_cb, sizeof(xdrc_preeq_cb),RES_BUF_XDRC_PRE_EQ,RES_LEN_XDRC_PRE_EQ);
#endif // BT_MUSIC_EFFECT_XDRC_EQ_EN

    xdrc_softeq_cb_init(xdrc_lp_cb,  sizeof(xdrc_lp_cb),  RES_BUF_XDRC_LP_EQ, RES_LEN_XDRC_LP_EQ);
    xdrc_softeq_cb_init(xdrc_lp_cb2, sizeof(xdrc_lp_cb2), RES_BUF_XDRC_LP_EQ, RES_LEN_XDRC_LP_EQ);
    xdrc_softeq_cb_init(xdrc_hp_cb,  sizeof(xdrc_hp_cb),  RES_BUF_XDRC_HP_EQ, RES_LEN_XDRC_HP_EQ);
    xdrc_softeq_cb_init(xdrc_hp_cb2, sizeof(xdrc_hp_cb2), RES_BUF_XDRC_HP_EQ, RES_LEN_XDRC_HP_EQ);
    //LP HP EXPAND FILTER EQ
    xdrc_softeq_cb_init(xdrc_lp_exp_cb, sizeof(xdrc_lp_exp_cb), RES_BUF_XDRC_LP_EXP_EQ, RES_LEN_XDRC_LP_EXP_EQ);
    xdrc_softeq_cb_init(xdrc_hp_exp_cb, sizeof(xdrc_hp_exp_cb), RES_BUF_XDRC_HP_EXP_EQ, RES_LEN_XDRC_HP_EXP_EQ);
    //DRC LP/HP
    xdrc_drcv1_cb_init(xdrc_drclp_cb, sizeof(xdrc_drclp_cb), RES_BUF_XDRC_LP_DRC, RES_LEN_XDRC_LP_DRC);
    xdrc_drcv1_cb_init(xdrc_drchp_cb, sizeof(xdrc_drchp_cb), RES_BUF_XDRC_HP_DRC, RES_LEN_XDRC_HP_DRC);

    //DRC ALL
    xdrc_drcv1_cb_init(xdrc_drcall_cb, sizeof(xdrc_drcall_cb), RES_BUF_XDRC_ALL_DRC, RES_LEN_XDRC_ALL_DRC);

    soft_vol_init(&xdrc_soft_vol);
}

AT(.text.xdrc)
void xdrc_set_vol(u16 vol, u8 vol_direct_set)
{
    soft_vol_set_vol_param(&xdrc_soft_vol, vol, vol_direct_set);
}

AT(.audio_text.xdrc)
s32 xdrc_process(s16 data)
{
    s32 pcm32;
    s32 pcm32_lp;
    s32 pcm32_hp;
    s32 pcm32_lp_exp, drc_gain_lp;
    s32 pcm32_hp_exp, drc_gain_hp;

    soft_vol_process_mono_one_sample(&xdrc_soft_vol, &data);
    pcm32 = (s32)data;

#if BT_MUSIC_EFFECT_XDRC_EQ_EN
    //软件EQ
    pcm32 = xdrc_softeq_proc(xdrc_preeq_cb,pcm32);
#endif // BT_MUSIC_EFFECT_XDRC_EQ_EN
    //LP/HP filter for DrcGain
    pcm32_lp_exp = xdrc_softeq_proc(xdrc_lp_exp_cb, pcm32);
    pcm32_hp_exp = xdrc_softeq_proc(xdrc_hp_exp_cb, pcm32);
    //expand freq to get drc_gain
    drc_gain_lp = xdrc_drc_process_gain(xdrc_drclp_cb, pcm32_lp_exp);
    drc_gain_hp = xdrc_drc_process_gain(xdrc_drchp_cb, pcm32_hp_exp);

#if BT_MUSIC_EFFECT_XDRC_DELAY_EN
    pcm32 = pcmdelay_mono_s32(xdrc_delay_cb, pcm32);
#endif // BT_MUSIC_EFFECT_XDRC_DELAY_EN

    //全频pcm32 过 drc_gain
    pcm32_lp = xdrc_drc_get_pcm32_s(pcm32, drc_gain_lp);
    pcm32_hp = xdrc_drc_get_pcm32_s(pcm32, drc_gain_hp);
    //LP/HP filter
    pcm32_lp = xdrc_softeq_proc(xdrc_lp_cb, pcm32_lp);
    pcm32_lp = xdrc_softeq_proc(xdrc_lp_cb2, pcm32_lp);
    pcm32_hp = xdrc_softeq_proc(xdrc_hp_cb, pcm32_hp);
    pcm32_hp = xdrc_softeq_proc(xdrc_hp_cb2, pcm32_hp);
    //LP/HP mix
    pcm32 = pcm32_lp + pcm32_hp;
    //all DRC
    pcm32 = xdrc_drc_process_s16(xdrc_drcall_cb, pcm32);

    return pcm32;
}

AT(.audio_text.xdrc)
void xdrc_frame_process(u32* buf, u32 samples, u32 ch_idx, u32 in_24bits)
{
    int i;
    s16 tmp16;
    s32 tmp32;
    s16 *pcm16 = (s16*)buf;
    s32 *pcm32 = (s32*)buf;

    if (in_24bits) {
        if (ch_idx == 1) {
            for (i = 0; i < samples; i++) {
                tmp16 = (s16)(pcm32[2*i+0]>>8);
                tmp32 = xdrc_process(tmp16);
                pcm32[2*i+0] = (s32)(tmp32<<8);
                pcm32[2*i+1] = (s32)(tmp32<<8);
            }
        } else {
            for (i = 0; i < samples; i++) {
                tmp16 = (s16)(pcm32[2*i+1]>>8);
                tmp32 = xdrc_process(tmp16);
                pcm32[2*i+0] = (s32)(tmp32<<8);
                pcm32[2*i+1] = (s32)(tmp32<<8);
            }
        }
    } else {
        if (ch_idx == 1) {
            for (i = 0; i < samples; i++) {
                tmp16 = pcm16[2*i+0];
                tmp32 = xdrc_process(tmp16);
                pcm16[2*i+0] = (s16)tmp32;
                pcm16[2*i+1] = (s16)tmp32;
            }
        } else {
            for (i = 0; i < samples; i++) {
                tmp16 = pcm16[2*i+1];
                tmp32 = xdrc_process(tmp16);
                pcm16[2*i+0] = (s16)tmp32;
                pcm16[2*i+1] = (s16)tmp32;
            }
        }
    }
}

void music_xdrc_audio_start_do(void)
{
    u8 vol_level = dac_dvol_table[sys_cb.vol] + sys_cb.gain_offset;

    if (vol_level > 60) {
        vol_level = 60;
    }
    dac_vol_set(DIG_N0DB);
    xdrc_set_vol(dac_dvol_tbl_db[vol_level], 1);
    xdrc_param_init();
}

void music_xdrc_audio_start(void)
{
    music_effect_t* cb = &music_effect;

    if (XDRC_EN(cb->music_effect_state_set)) {
        return;
    }

    cb->music_effect_state_set |= XDRC_STA_BIT;

    if (cb->audio_comm_init_flag) {         //已初始化audio comm，直接开启
        music_xdrc_audio_start_do();
        music_effect_set_state(MUSIC_EFFECT_XDRC, 1);
    }

    TRACE("music_xdrc_start\n");
}

void music_xdrc_audio_stop_do(void)
{
    u8 vol_level = dac_dvol_table[sys_cb.vol] + sys_cb.gain_offset;
    dac_vol_set(dac_dvol_tbl_db[vol_level]);
}

void music_xdrc_audio_stop(void)
{
    music_effect_t* cb = &music_effect;

    if (XDRC_EN(cb->music_effect_state_set) == 0) {
        return;
    }

    cb->music_effect_state_set &= ~XDRC_STA_BIT;

    music_xdrc_audio_stop_do();

    music_effect_set_state(MUSIC_EFFECT_XDRC, 0);

    TRACE("music_xdrc_stop\n");
}

void music_xdrc_audio_set_vol_do(u8 vol_level)
{
    xdrc_set_vol(dac_dvol_tbl_db[vol_level], 0);
}

#if BT_MUSIC_EFFECT_XDRC_DELAY_EN
void music_xdrc_set_delay(u16 delay_samples)
{
    pcmdelay_coef_update(xdrc_delay_cb,delay_samples);
}
#endif // BT_MUSIC_EFFECT_XDRC_DELAY_EN

#endif // BT_MUSIC_EFFECT_XDRC_EN

///用户自定义音效算法
#if BT_MUSIC_EFFECT_USER_EN

AT(.audio_text.user)
void alg_user_effect_process(u8 *buf, u32 samples, u32 nch, u32 is_24bit, u32 pcm_info)
{
    //用户自定义音效算法处理示例函数
    //算法中用到的函数可以放在audio_text段或者com_text段，以提高算法效率
    //算法中用到的buffer需要放到music_buff段或者music_exbuff段

}

void alg_user_effect_init(void)
{
    //用户自定义音效算法初始化示例函数
    printf("alg_user_effect_init\n");

}

void music_effect_alg_user_start(void)
{
    music_effect_t* cb = &music_effect;

    if (USER_ALG_EN(cb->music_effect_state_set)) {
        return;
    }

    cb->music_effect_state_set |= USER_ALG_STA_BIT;

    if (cb->audio_comm_init_flag) {         //已初始化audio comm，直接开启
        alg_user_effect_init();
        music_effect_set_state(MUSIC_EFFECT_ALG_USER, 1);
    }

    TRACE("music_effect_alg_user_start\n");
}

void music_effect_alg_user_stop(void)
{
    music_effect_t* cb = &music_effect;

    if (USER_ALG_EN(cb->music_effect_state_set) == 0) {
        return;
    }

    cb->music_effect_state_set &= ~USER_ALG_STA_BIT;

    music_effect_set_state(MUSIC_EFFECT_ALG_USER, 0);

    TRACE("music_effect_alg_user_stop\n");
}
#endif // BT_MUSIC_EFFECT_USER_EN

#endif // BT_MUSIC_EFFECT_EN
