#ifndef __BSP_ANC_ALG_H__
#define __BSP_ANC_ALG_H__

u32 bsp_anc_alg_get_type(void);
void bsp_anc_alg_start(u32 type);
void bsp_anc_alg_stop(void);
void anc_alg_process(void);

///在线调试
bool bsp_anc_alg_dbg_rx_done(u8* rx_buf, u8 type);
void bsp_anc_alg_parse_cmd(void);

#endif // __BSP_ANC_ALG_H__
