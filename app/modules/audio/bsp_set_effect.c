#include "include.h"

#if BT_MUSIC_EFFECT_DBG_EN

#define XTP_EFFECT_FRAME_HEAD_TAG           12
#define XTP_EFFECT_NAME_TAG                 8

#define CAR_TOOL_SEED                       0xffff

uint calc_crc(void *buf, uint len, uint seed);
void tx_ack(uint8_t *packet, uint16_t len);
u8 check_sum(u8 *buf, u16 size);


u8 effect_tx_ack_buf[14];


//跟数组 bsp_effect_name 一一对应，请同步修改，顺序也不要弄乱
enum BSP_EFFECT_CH {
    BSP_EFFECT_VBASS,
    BSP_EFFECT_DYEQ,
    BSP_EFFECT_DYEQ_DRC,
    BSP_EFFECT_XDRC_DELAY,

    BSP_EFFECT_NUM,
};

//name最长8个字符，跟CAR_Tool里模块的名字一一对应
static const char bsp_effect_name[][9] = {
    "VBAS",
    "DYNEQ",
    "DYEQ_DRC",
    "DELA",
};

void bsp_set_effect(u8 effect_id, u8* buf)
{
    print_r(buf, 20);

    switch (effect_id) {
#if BT_MUSIC_EFFECT_VBASS_EN || BT_MUSIC_EFFECT_DYEQ_VBASS_EN
        case BSP_EFFECT_VBASS:
            printf("cutoff_frequency : %d\n", buf[2]);
            printf("intensity : %x\n", little_endian_read_32(buf, 3));
            printf("high_frequency : %d\n", buf[7]);
            printf("pregain : %x\n", little_endian_read_32(buf, 8));
            music_vbass_set_param(buf[2], little_endian_read_32(buf, 3), buf[7], little_endian_read_32(buf, 8));
            break;
#endif // BT_MUSIC_EFFECT_VBASS_EN

#if BT_MUSIC_EFFECT_DYEQ_EN
        case BSP_EFFECT_DYEQ:
            printf("gain       : %x\n", little_endian_read_32(buf, 2));
            printf("threshold1 : %x\n", little_endian_read_32(buf, 6));
            printf("threshold2 : %x\n", little_endian_read_32(buf, 10));
            printf("slop       : %x\n", little_endian_read_32(buf, 14));
            music_dyeq_set_param(&buf[2]);
            printf("BSP_EFFECT_DYEQ\n");
            break;

        case BSP_EFFECT_DYEQ_DRC:
            music_dyeq_drc_set_param(&buf[6]);
            printf("BSP_EFFECT_DYEQ_DRC\n");
            break;
#endif // BT_MUSIC_EFFECT_DYEQ_EN

#if BT_MUSIC_EFFECT_XDRC_DELAY_EN
        case BSP_EFFECT_XDRC_DELAY:
            printf("delaysamples = %d\n", little_endian_read_16(buf, 2));
            music_xdrc_set_delay(little_endian_read_16(buf, 2));
            printf("BSP_EFFECT_XDRC_DELAY\n");
            break;
#endif // BT_MUSIC_EFFECT_XDRC_EN

        default:
            break;
    }
}

void bsp_effect_tx_ack(u8 *head_tag, u8 ack)
{
    head_tag[12] = ack;
    head_tag[13] = check_sum(head_tag, 13);
    tx_ack(head_tag, 14);
}

void bsp_effect_parse_cmd(void)
{
//    printf("-->%s\n", __func__);
//    print_r(eq_rx_buf, 20);
    int i;
    u8 name_len = 8;
    u8 name_8byte[8];
    u8* name = &eq_rx_buf[4];                   //把“CAR_”截掉，只获取后面的模块名字
    u16 size = little_endian_read_16(eq_rx_buf, 12);
    u32 crc = calc_crc(eq_rx_buf, size+XTP_EFFECT_FRAME_HEAD_TAG, CAR_TOOL_SEED);

    for(i = 0; i < XTP_EFFECT_NAME_TAG; i++) {     //获取name有效字符长度
        if(name[i]=='#') {
            name_len = i;
            break;
        }
    }
    memcpy(effect_tx_ack_buf, eq_rx_buf, 12);

    if (crc != little_endian_read_16(eq_rx_buf, size+XTP_EFFECT_FRAME_HEAD_TAG)) {
        bsp_effect_tx_ack(effect_tx_ack_buf, 1);
        memset(eq_rx_buf, 0, EQ_BUFFER_LEN);
        printf("-->CRC_ERROR %x %x %d\n", crc, little_endian_read_16(eq_rx_buf, size+XTP_EFFECT_FRAME_HEAD_TAG), size+XTP_EFFECT_FRAME_HEAD_TAG);
        return;
    }

    memcpy(name_8byte, name, name_len);
    name_8byte[name_len] = 0;
    printf("CAR_RESEVE:%s, len:%d\n", name_8byte, name_len);

    for (i = 0; i < BSP_EFFECT_NUM; i++) {
        if (0 == memcmp((char *)name, &bsp_effect_name[i], name_len)) {
//            printf("res___%d\n", i);
            bsp_set_effect(i, &eq_rx_buf[14]);
            bsp_effect_tx_ack(effect_tx_ack_buf, 0);
            break;
        }
    }

    memset(eq_rx_buf, 0, EQ_BUFFER_LEN);
}

#if BT_MUSIC_EFFECT_XDRC_EN

#define XDRC_EQ_NUM     9
extern soft_vol_t xdrc_soft_vol AT(.music_buff.xdrc);
#if BT_MUSIC_EFFECT_XDRC_EQ_EN
extern s32 xdrc_preeq_cb[BT_MUSIC_EFFECT_XDRC_EQ_BAND*7 + 6];
#endif
extern s32 xdrc_lp_cb[1*7 + 6];
extern s32 xdrc_lp_cb2[1*7 + 6];
extern s32 xdrc_hp_cb[1*7 + 6];
extern s32 xdrc_hp_cb2[1*7 + 6];
extern s32 xdrc_lp_exp_cb[1*7 + 6];
extern s32 xdrc_hp_exp_cb[1*7 + 6];
extern u8 xdrc_drclp_cb[14 * 4];
extern u8 xdrc_drchp_cb[14 * 4];
extern u8 xdrc_drcall_cb[14 * 4];

enum {
    XDRC_NONE,
    XDRC_PRE_EQ,
    XDRC_LP_EQ,
    XDRC_LP_EXP_EQ,
    XDRC_HP_EQ,
    XDRC_HP_EXP_EQ,
    XDRC_LP_DRC,
    XDRC_HP_DRC,
    XDRC_ALL_DRC,
};

static const char xdrc_eq_name[XDRC_EQ_NUM][10] = {
    "NONE",
    "PRE.EQ",
    "LP.EQ",
    "LP_EXP.EQ",
    "HP.EQ",
    "HP_EXP.EQ",
    "LP.DRC",
    "HP.DRC",
    "ALL.DRC",
};

AT(.text.xdrc)
u8 xdrc_eq_name_compare(const char *eq_name)
{
    printf("%s : %s\n", __func__, eq_name);
    u32 i, num;
    const char (*xdrc_name)[10];

    num = XDRC_EQ_NUM;
    xdrc_name = xdrc_eq_name;
    for (i = 1; i < num; i++) {
        if (strcmp(xdrc_name[i], eq_name) == 0) {
            return i;
        }
    }
    return 0;
}

AT(.text.xdrc)
void bsp_xdrc_eq_param(u8 packet, u8 band_cnt, u8* eq_buf)
{
    switch(packet) {
    #if BT_MUSIC_EFFECT_XDRC_EQ_EN
        case XDRC_PRE_EQ:
            band_cnt = (eq_buf[7] > BT_MUSIC_EFFECT_XDRC_EQ_BAND)?BT_MUSIC_EFFECT_XDRC_EQ_BAND:eq_buf[7];
            xdrc_softeq_coef_update(xdrc_preeq_cb, sizeof(xdrc_preeq_cb),(u32 *)&eq_buf[14],band_cnt,true);
            break;
    #endif // BT_MUSIC_EFFECT_XDRC_EQ_EN

        case XDRC_LP_EQ:
            xdrc_softeq_coef_update(xdrc_lp_cb,sizeof(xdrc_lp_cb),(u32 *)&eq_buf[14],1,true);
            xdrc_softeq_coef_update(xdrc_lp_cb2,sizeof(xdrc_lp_cb2),(u32 *)&eq_buf[14],1,true);
            printf("XDRC_LP_EQ\n");
            break;

        case XDRC_LP_EXP_EQ:
            xdrc_softeq_coef_update(xdrc_lp_exp_cb,sizeof(xdrc_lp_exp_cb),(u32 *)&eq_buf[14],1,true);
            printf("XDRC_LP_EXP_EQ\n");
            break;

        case XDRC_HP_EQ:
            xdrc_softeq_coef_update(xdrc_hp_cb,sizeof(xdrc_hp_cb),(u32 *)&eq_buf[14],1,true);
            xdrc_softeq_coef_update(xdrc_hp_cb2,sizeof(xdrc_hp_cb2),(u32 *)&eq_buf[14],1,true);
            printf("XDRC_HP_EQ\n");
            break;

        case XDRC_HP_EXP_EQ:
            xdrc_softeq_coef_update(xdrc_hp_exp_cb,sizeof(xdrc_hp_exp_cb),(u32 *)&eq_buf[14],1,true);
            printf("XDRC_HP_EXP_EQ\n");
            break;

        case XDRC_LP_DRC:
            xdrc_drcv1_coef_update(xdrc_drclp_cb, (u32*)&eq_buf[10]);
            printf("XDRC_LP_DRC\n");
            break;

        case XDRC_HP_DRC:
            xdrc_drcv1_coef_update(xdrc_drchp_cb, (u32*)&eq_buf[10]);
            printf("XDRC_HP_DRC\n");
            break;

        case XDRC_ALL_DRC:
            xdrc_drcv1_coef_update(xdrc_drcall_cb, (u32*)&eq_buf[10]);
            printf("XDRC_ALL_DRC\n");
            break;

        default:
            break;
    }
}
#endif
#endif
