#include "include.h"


#if ANC_ALG_EN

#define TRACE_EN                    0

#if TRACE_EN
#define TRACE(...)                  printf(__VA_ARGS__)
#else
#define TRACE(...)
#endif // TRACE_EN


//算法公共参数
typedef struct {
    u8    type;
    u8    mic_cfg;
    u8    pcm_interleave;
    u16   alg_samples;
    u8    alg_nch;
    u16   sdadc_ch;
    u16   sdadc_samples;
    void* alg_param;
} anc_alg_comm_param_cb;

//声加泄露补偿自适应ANC算法(SAE_EL01)参数
typedef struct {
    int SHIELD_LEAKGEAR[7];
    int SHIELD_CORR_CORRECT;
    int SHIELD_ShieldStartFrm;
    int SHIELD_ShieldEndFrm;
    int SHIELD_ShieldOverFrm;
    int SHIELD_ShieldHalfDetFrm;
} g_Cfg_SpEc_ShieldLeakageDetect;

//声加泄露补偿自适应ANC算法(SAE_EL01)参数
typedef struct {
    int DataScaleff;
    int DataScalefb;
    int THR_FF;
    int THR_FB;
    int THR_Est0;
    int THR_Est1;
    int THR_Est2;
    int THR_Est3;
    int THR_Est4;
    int THR_Est5;
    short GainMax;
    short GainMin;
    short ModeMax;
} g_Cfg_SpEc_AdapANC;

typedef struct {
    int FFGain;
    int FBGain;
    int ATT_WindFFFB_FrmNum;
    int DEC_WindFFFB_FrmNum;
    int WindFFFB_TH;
    int noWindFFFB_TH;
    int Hcomp[4][34];
    int Hoff[4][34];
    int PWRDIFTHH[4];
} g_Cfg_SpEc_WindDetect;

void anc_alg_ram_clear(void);
int anc_alg_init(anc_alg_param_cb* p);
int anc_alg_exit(anc_alg_param_cb* p);
uint calc_crc(void *buf, uint len, uint seed);


#if ANC_SNDP_SAE_SHIELD_ADAPTER_EN

#define _IQ24(A)                ((int)((A) * ((int)1 << 24)))
#define _IQ21(A)                ((int)((A) * ((int)1 << 21)))

AT(.sndp_sae_rodata.SAE_EL01)
g_Cfg_SpEc_ShieldLeakageDetect gc_Para_ShieldLeakDet_Cfg = {
    .SHIELD_LEAKGEAR = {97000,77000,59800,44731,35000,30000,15000},
    .SHIELD_CORR_CORRECT = _IQ24(31.36),
    .SHIELD_ShieldStartFrm = 228,
    .SHIELD_ShieldEndFrm = 234,
    .SHIELD_ShieldOverFrm = 237,
    .SHIELD_ShieldHalfDetFrm = 7,
};

AT(.sndp_sae_rodata.SAE_EL01)
g_Cfg_SpEc_AdapANC gc_Para_AdapANC_Cfg = {
    .DataScaleff = _IQ24(0.1), // 不要低于 0.1
    .DataScalefb = _IQ24(0.75),
    .THR_FF = _IQ21(5.0f),
    .THR_FB = _IQ21(-10.0f),
    .THR_Est0 = _IQ21(2.0f),
    .THR_Est1 = _IQ21(18.0f),
    .THR_Est2 = _IQ21(5.0f),
    .THR_Est3 = _IQ21(16.0f),
    .THR_Est4 = _IQ21(17.0f),
    .THR_Est5 = _IQ21(10.5f),
    .GainMax = 0 * 64,
    .GainMin = -4 * 64,
    .ModeMax = 5,
};

int FB_total_gain = 321;
#endif // ANC_SNDP_SAE_SHIELD_ADAPTER_EN

#if ANC_SNDP_SAE_WIND_DETECT_EN

#define _IQ25(A)                ((int)((A) * ((int)1 << 25)))
#define _IQ30(A)                ((int)((A) * ((int)1 << 30)))

AT(.sndp_sae_rodata.dwind)
const g_Cfg_SpEc_WindDetect gc_Para_WindDetect_Cfg = {
    // Calibration gain for FF and FB mic signals.
    .FFGain = _IQ25(17.5792f),      // +8.8dB ( 10^(+8.8/20) = 2.7542 )
    .FBGain = _IQ25(1.7783f),      // +1dB ( 10^(+1/20) = 1.2220 )

    .ATT_WindFFFB_FrmNum = 135,
    .DEC_WindFFFB_FrmNum = 600,

    .WindFFFB_TH = _IQ30(0.55f),
    .noWindFFFB_TH = _IQ30(0.1f),

    .Hcomp = {
        {    // Only spectrum below 500Hz is used. Transparancy mode (with compensation for FB銆€ANC) for ziyan earphone.
            -64497159, 973517474, -46210455,  29241855, -14855326,  26127703,  -5476212,   2420901, -16422022, -10319346,
            -22801208,  -8209959, -19622954,  -9969027, -21782378, -19612238, -32361140, -24275477, -40094765, -21425144,
            -43356557, -20850606, -51332590, -22833442, -63558880, -17690154, -69886033,  -5291593, -69098636,   5087389,
            -68493927,  12340450, -67316590,  22402872
        },
        {   // Only spectrum below 500Hz is used. Strong ANC mode for ziyan earphone.
            -83659349, 325611177, -66018007,  30275697, -33746675,  30051843, -19962514,   6775569, -28371308, -11148726,
            -38993987, -14671973, -43166333, -16494349, -50185533, -22541341, -64453543, -24024265, -77758278, -16093006,
            -84895913,  -4769325, -89480511,   6451685, -91917411,  20973163, -86420197,  37373238, -73343825,  47099857,
            -61947795,  47627043, -57037488,  46589744
        },
        {   // Only spectrum below 500Hz is used. Middle ANC mode for ziyan earphone.
            -15791929, -97130471, -19621773, -18660709, -32158353, -35170456, -53422822, -43958754, -77527982, -38970300,
            -94085747, -20718063, -96552926,   1894789, -87791666,  19093750, -75246775,  28295996, -62445453,  32343724,
            -48811573,  31979639, -36189094,  25122595, -29502667,  13267455, -29742326,   2478432, -31890282,  -4237758,
            -32598072, -10489878, -35138523, -19357396
        },
        {   // Only spectrum below 500Hz is used. Light ANC mode for ziyan earphone.
            44661217,  51119989,  35422915,   8624165,  26277941,  40596009,  46335911,  83749564,  94748246, 105406057,
            143165047,  92869970, 171044066,  61860328, 180651113,  29930339, 179475473,    228041, 166495051, -27116469,
            141001264, -44393018, 111705163, -44546821,  89479309, -30360394,  78299251,  -9463681,  77751449,  12996893,
            87360773,  32880702, 104166916,  45434720
        },
    },

    .Hoff = {
        {   // Only spectrum below 500Hz is used. FF ANC off (FB ANC on only) mode for ziyan earphone.
            68243380, 702547923,  49577280, -29561036,  17190628, -26050802,   7236011,   -581904,  19528572,  14030756,
            27997668,  11650120,  24680557,  11919243,  24984254,  22457976,  36085413,  30453723,  47392666,  28995990,
            52761397,  26673164,  59729530,  28511174,  72982726,  27152633,  85922410,  17086239,  92511019,   2460823,
            93784970, -12380565,  90204235
        },
        {   // Only spectrum below 500Hz is used. FF ANC off (FB ANC on only) mode for ziyan earphone.
            112680614, -373663250,   83682155,  -48516294,   32926486,  -45932184,   16193809,   -8788899,   34057179,   12776368,
            47495488,    8742277,   44688405,    7908710,   48685194,   20419198,   68869264,   25507897,   86693228,   12563746,
            90542764,   -3886817,   90501768,  -13682649,   93349096,  -24619182,   90838644,  -40755245,   79356457,  -52646700,
            68169979,  -55849457,   61750592,  -58139469
        },
        {   // Only spectrum below 500Hz is used. FF ANC off (FB ANC on only) mode for ziyan earphone.
            110027806, 109160996, 104737953, -15531418,  94854202, -22502171,  89634249, -23566142,  89024386, -27055996,
            86054373, -34233101,  78905529, -39050079,  73167753, -39168448,  71926486, -39821498,  70442360, -45024443,
            63640420, -50814617,  54225100, -51075322,  48969435, -46716746,  48994818, -44455875,  48322591, -47257504,
            42511221, -50306845,  34904748, -48024520
        },
        {   // Only spectrum below 500Hz is used. FF ANC off (FB ANC on only) mode for ziyan earphone.
            140515460, 400189913, 120072017, -30389771,  87940765, -22339800,  86925972,   7128357, 112360274,  16685089,
            129502219,   1712683, 129855738, -11872643, 132158442, -16827424, 140238155, -28761678, 138249543, -49356025,
            124134283, -61902999, 114034667, -62801978, 112302507, -66415463, 106059278, -76578408,  92265561, -80762565,
            83783344, -76069862,  84015681, -75635534
        },
    },
    .PWRDIFTHH = {
        214748, 1073742, 4294967,  711032
    },
};
#endif // SNDP_SAE_WIND_DETECT_EN

#define SNDP_SAE_SHIELD_LEAK_CH     (CH_MIC3)                   //FB
#define SNDP_SAE_ADAPTER_ANC_CH     (CH_MIC2 | CH_MIC3 << 8)    //FF + FB
#define SNDP_SAE_DWIND_ANC_CH       (CH_MIC2 | CH_MIC3 << 8)    //FF + FB

const anc_alg_comm_param_cb anc_alg_comm_param_tbl[] = {
/*  type                        mic_cfg     pcm_interleave  alg_samples alg_nch sdadc_ch                    sdadc_samples   alg_param*/
#if ANC_SNDP_SAE_SHIELD_ADAPTER_EN
    {SNDP_SAE_SHIELD_LEAK,      0,          0,              120,        1,      SNDP_SAE_SHIELD_LEAK_CH,    240,             &FB_total_gain},
    {SNDP_SAE_ADAPTER_ANC,      0,          1,              128,        2,      SNDP_SAE_ADAPTER_ANC_CH,    256,             0},
#endif // ANC_SNDP_SAE_SHIELD_ADAPTER_EN
#if ANC_SNDP_SAE_WIND_DETECT_EN
    {SNDP_SAE_WIND_DETECT,      0,          1,              240,        2,      SNDP_SAE_DWIND_ANC_CH,      480,             0},
#endif // SNDP_SAE_WIND_DETECT_EN
};

anc_alg_param_cb ada_param_cb AT(.anc_data.ada_param_cb);
u8 anc_alg_param_idx;

static s8 get_anc_alg_param_idx_by_type(u8 type)
{
    int total = sizeof(anc_alg_comm_param_tbl) / sizeof(anc_alg_comm_param_cb);
//    printf("total %d %d %d\n", total, sizeof(anc_alg_comm_param_tbl), sizeof(anc_alg_comm_param_cb));
    for (int i = 0; i < total; i++) {
        if (type == anc_alg_comm_param_tbl[i].type) {
            return i;
        }
    }
    return -1;
}

u32 bsp_anc_alg_get_type(void)
{
    u32* tptr = (u32*)ada_param_cb.type;
    u32 type_all = *tptr;
    return type_all;
}

void bsp_anc_alg_start(u32 type)
{
#if BT_A2DP_LHDC_AUDIO_EN
    if (bt_decode_is_lhdc()) {
        return;
    }
#endif // BT_A2DP_LHDC_AUDIO_EN

#if BT_A2DP_LDAC_AUDIO_EN
    if (bt_decode_is_ldac()) {
        return;
    }
#endif // BT_A2DP_LDAC_AUDIO_EN

    if (sys_cb.anc_alg_en == 1) {
        return;
    }

    if (type == 0) {
        return;
    }

    //参数初始化
    anc_alg_ram_clear();
    anc_alg_param_idx = 0;
    u8 temp_idx = 0;
    for (int i = 0; i < 4; i++) {
        if ((type >> (8 * i)) & 0xFF) {
            ada_param_cb.type[i] = (u8)((type >> (8 * i)) & 0xFF);
            temp_idx = get_anc_alg_param_idx_by_type(ada_param_cb.type[i]);
            if (temp_idx < 0) {
                printf("ERROR: %s: init param error!\n", __func__);
                return;
            }
            ada_param_cb.alg_param[i] = anc_alg_comm_param_tbl[temp_idx].alg_param;
            if (i == 0) {
                anc_alg_param_idx = temp_idx;
            }
            TRACE("ada_param_cb type[%d]: %x alg_param %08x\n", i, ada_param_cb.type[i], ada_param_cb.alg_param[i]);
        } else {
            break;
        }
    }
    ada_param_cb.mic_cfg = anc_alg_comm_param_tbl[anc_alg_param_idx].mic_cfg;   //以第一个type的为准
    ada_param_cb.pcm_interleave = anc_alg_comm_param_tbl[anc_alg_param_idx].pcm_interleave;
    ada_param_cb.alg_samples = anc_alg_comm_param_tbl[anc_alg_param_idx].alg_samples;
    ada_param_cb.alg_nch = anc_alg_comm_param_tbl[anc_alg_param_idx].alg_nch;
    ada_param_cb.dump_en = ANC_ALG_DUMP_EN;
    ada_param_cb.start_delay = 0;
    TRACE("ada_param_cb mic_cfg: %d\n", ada_param_cb.mic_cfg);
    TRACE("ada_param_cb pcm_interleave: %d\n", ada_param_cb.pcm_interleave);
    TRACE("ada_param_cb alg_samples: %d\n", ada_param_cb.alg_samples);
    TRACE("ada_param_cb alg_nch: %d\n", ada_param_cb.alg_nch);
    TRACE("ada_param_cb dump_en: %d\n", ada_param_cb.dump_en);
    TRACE("ada_param_cb start_delay: %d\n", ada_param_cb.start_delay);

    //算法初始化
    if (anc_alg_init(&ada_param_cb) < 0) {
        printf("ERROR: %s: anc_alg_init error!\n", __func__);
        return;
    }

    //调整主频
    if (ada_param_cb.type[0] == SNDP_SAE_ADAPTER_ANC) {
        sys_clk_req(INDEX_ANC, SYS_60M);
    } else {
        sys_clk_req(INDEX_ANC, SYS_48M);
    }

    //SDADC初始化
    audio_path_init(AUDIO_PATH_ANC_ALG);
    audio_path_start(AUDIO_PATH_ANC_ALG);

    sys_cb.anc_alg_en = 1;

    TRACE("ANC alg: start\n");
}

void bsp_anc_alg_stop(void)
{
    if (sys_cb.anc_alg_en == 0) {
        return;
    }

    sys_cb.anc_alg_en = 0;

    //算法关闭
    anc_alg_exit(&ada_param_cb);

    //SDADC关闭
    audio_path_exit(AUDIO_PATH_ANC_ALG);

    //调整主频
    sys_clk_free(INDEX_ANC);

    TRACE("ANC alg: stop\n");
}

//ANC算法主线程process
AT(.com_text.anc.alg_process)
void anc_alg_process(void)
{
    if (sys_cb.anc_alg_en == 1) {

    }
}

//ANC算法结果回调函数
AT(.anc_text.process.comm)
void alg_anc_process_callback(int* res, u32 len, u8 type)
{
#if ANC_SNDP_SAE_SHIELD_ADAPTER_EN
    if (type == SNDP_SAE_SHIELD_LEAK) {
//        printf("leakaga_level: %d, anc_mode: %d, frm_cnt: %d res: %d\n", res[0], res[1], res[2], res[3]);
    }
    if (type == SNDP_SAE_ADAPTER_ANC) {
//        printf("anc_mode: %d, ff_total_gain: %d, frm_cnt: %d res: %d\n", res[0], res[1], res[2], res[3]);
    }
#endif // ANC_SNDP_SAE_SHIELD_ADAPTER_EN
#if ANC_SNDP_SAE_WIND_DETECT_EN
    if (type == SNDP_SAE_WIND_DETECT) {
//        printf("wind_level: %d, frm_cnt: %d\n", res[0], res[1]);
    }
#endif // SNDP_SAE_WIND_DETECT_EN
}

void anc_alg_audio_path_cfg_set(sdadc_cfg_t* cfg)
{
    cfg->channel = anc_alg_comm_param_tbl[anc_alg_param_idx].sdadc_ch;
    cfg->samples = anc_alg_comm_param_tbl[anc_alg_param_idx].sdadc_samples;

    //增益设置
    cfg->anl_gain = ((xcfg_cb.mic0_anl_gain)     |
                     (xcfg_cb.mic1_anl_gain<<6)  |
                     (xcfg_cb.mic2_anl_gain<<12) |
                     (xcfg_cb.mic3_anl_gain<<18) |
                     (xcfg_cb.mic4_anl_gain<<24));
    if (xcfg_cb.anc_alg_dgain_en) {
        cfg->dig_gain = ((xcfg_cb.anc_mic0_dig_gain)      |
                         (xcfg_cb.anc_mic1_dig_gain<<6)   |
                         (xcfg_cb.anc_mic2_dig_gain<<12)  |
                         (xcfg_cb.anc_mic3_dig_gain<<18)  |
                         (xcfg_cb.anc_mic4_dig_gain<<24));
    } else {
        cfg->dig_gain = ((xcfg_cb.bt_mic0_dig_gain)       |
                         (xcfg_cb.bt_mic1_dig_gain<<6)    |
                         (xcfg_cb.bt_mic2_dig_gain<<12)   |
                         (xcfg_cb.bt_mic3_dig_gain<<18)   |
                         (xcfg_cb.bt_mic4_dig_gain<<24));
    }

//    printf("cfg->channel %x\n", cfg->channel);
//    printf("cfg->samples %x\n", cfg->samples);
//    printf("cfg->anl_gain %x\n", cfg->anl_gain);
//    printf("cfg->dig_gain %x\n", cfg->dig_gain);
}

///在线调试
#if ANC_ALG_DBG_EN

#define SAL_VERSION             '1'
#define ANC_ALG_CRC_SEED        0xFFFF

enum {
    ANC_ALG_DBG_ACK_OK = 0,
    ANC_ALG_DBG_ACK_DATA_CRC_ERR,
    ANC_ALG_DBG_ACK_DATA_LEN_ERR,
    ANC_ALG_DBG_ACK_ALG_TYPE_ERR,
    ANC_ALG_DBG_ACK_ALG_PARAM_ERR,
};

u8 anc_alg_dbg_buf[10];
u8 anc_alg_dbg_type;


static u8 check_sum(u8* buf, u16 size)
{
    u32 i, sum = 0;
    for (i = 0; i < size; i++) {
        sum += buf[i];
    }
    return (u8)(-sum);
}

static void tx_ack(u8* packet, u16 len)
{
    delay_5ms(1);   //延时一段时间再ack
    if (anc_alg_dbg_type == 0) {
#if HUART_EN
        huart_tx(packet, len);
#endif // HUART_EN
    } else if (anc_alg_dbg_type == 1) {
#if BT_SPP_EN
        if (bt_get_status() >= BT_STA_CONNECTED) {
//            printf("tx:\n");
//            print_r(packet, len);
            bt_spp_tx(SPP_SERVICE_CH0, packet, len);
        }
#endif // BT_SPP_EN
    }
}

AT(.com_text.anc_alg_dbg)
bool bsp_anc_alg_dbg_rx_done(u8* rx_buf, u8 type)
{
//    print_r(eq_rx_buf, EQ_BUFFER_LEN);
    if ((rx_buf[0] == 'S') && (rx_buf[1] == 'A') && (rx_buf[2] == 'L')) {
        anc_alg_dbg_type = type;
        if (type == 0) {    //HUART
            msg_enqueue(EVT_ONLINE_SET_ANC_ALG);
        }
        return true;
    }

    return false;
}

void bsp_anc_alg_dbg_tx_ack(u8 ack)
{
    anc_alg_dbg_buf[0] = 'S';
    anc_alg_dbg_buf[1] = 'A';
    anc_alg_dbg_buf[2] = 'L';
    anc_alg_dbg_buf[3] = SAL_VERSION;
    anc_alg_dbg_buf[4] = ack;
    anc_alg_dbg_buf[5] = check_sum(anc_alg_dbg_buf, 5);
    tx_ack(anc_alg_dbg_buf, 6);
}

void bsp_anc_alg_parse_cmd(void)
{
#if TRACE_EN
//    printf("EVT_ONLINE_SET_ANC_ALG:\n");
//    print_r(eq_rx_buf, EQ_BUFFER_LEN);
#endif // TRACE_EN

    if ((eq_rx_buf[0] != 'S') || (eq_rx_buf[1] != 'A') || (eq_rx_buf[2] != 'L')) {
        return;
    }

    u32 param_len = little_endian_read_32(eq_rx_buf, 16);
    if ((param_len + 22) > EQ_BUFFER_LEN) {
        bsp_anc_alg_dbg_tx_ack(ANC_ALG_DBG_ACK_DATA_LEN_ERR);
        return;
    }

    u16 crc_rx = little_endian_read_16(eq_rx_buf, 20 + param_len);
    u16 crc_cal = calc_crc(eq_rx_buf, 20 + param_len, ANC_ALG_CRC_SEED);
    if (crc_cal != crc_rx) {
        bsp_anc_alg_dbg_tx_ack(ANC_ALG_DBG_ACK_DATA_CRC_ERR);
        return;
    }

    bool alg_en = false;
#if ANC_SNDP_SAE_SHIELD_ADAPTER_EN
    if (strcmp((const char*)&eq_rx_buf[4], "SAE_EL01") == 0) {
        if (param_len == (sizeof(gc_Para_ShieldLeakDet_Cfg) + sizeof(gc_Para_AdapANC_Cfg))) {
            if (sys_cb.anc_alg_en) {
                bsp_anc_alg_stop();
            }
            bsp_anc_alg_start(SNDP_SAE_ADAPTER_ANC);
            memcpy(&gc_Para_ShieldLeakDet_Cfg, &eq_rx_buf[20], sizeof(gc_Para_ShieldLeakDet_Cfg));
            memcpy(&gc_Para_AdapANC_Cfg, &eq_rx_buf[20] + sizeof(gc_Para_ShieldLeakDet_Cfg), sizeof(gc_Para_AdapANC_Cfg));
//            printf("---------------------------------\n");
//            printf("gc_Para_ShieldLeakDet_Cfg:\n");
//            printf(" SHIELD_LEAKGEAR: "); for(int i = 0; i < 7; i++) {printf("%d ", gc_Para_ShieldLeakDet_Cfg.SHIELD_LEAKGEAR[i]);}; printf("\n");
//            printf(" SHIELD_CORR_CORRECT: %d\n", gc_Para_ShieldLeakDet_Cfg.SHIELD_CORR_CORRECT);
//            printf(" SHIELD_ShieldStartFrm: %d\n", gc_Para_ShieldLeakDet_Cfg.SHIELD_ShieldStartFrm);
//            printf(" SHIELD_ShieldEndFrm: %d\n", gc_Para_ShieldLeakDet_Cfg.SHIELD_ShieldEndFrm);
//            printf(" SHIELD_ShieldOverFrm: %d\n", gc_Para_ShieldLeakDet_Cfg.SHIELD_ShieldOverFrm);
//            printf(" SHIELD_ShieldHalfDetFrm: %d\n", gc_Para_ShieldLeakDet_Cfg.SHIELD_ShieldHalfDetFrm);
//            printf("gc_Para_AdapANC_Cfg:\n");
//            printf(" DataScaleff: %d\n", gc_Para_AdapANC_Cfg.DataScaleff);
//            printf(" DataScalefb: %d\n", gc_Para_AdapANC_Cfg.DataScalefb);
//            printf(" THR_FF: %d\n", gc_Para_AdapANC_Cfg.THR_FF);
//            printf(" THR_FB: %d\n", gc_Para_AdapANC_Cfg.THR_FB);
//            printf(" THR_Est0: %d\n", gc_Para_AdapANC_Cfg.THR_Est0);
//            printf(" THR_Est1: %d\n", gc_Para_AdapANC_Cfg.THR_Est1);
//            printf(" THR_Est2: %d\n", gc_Para_AdapANC_Cfg.THR_Est2);
//            printf(" THR_Est3: %d\n", gc_Para_AdapANC_Cfg.THR_Est3);
//            printf(" THR_Est4: %d\n", gc_Para_AdapANC_Cfg.THR_Est4);
//            printf(" THR_Est5: %d\n", gc_Para_AdapANC_Cfg.THR_Est5);
//            printf(" GainMax: %d\n", gc_Para_AdapANC_Cfg.GainMax);
//            printf(" GainMin: %d\n", gc_Para_AdapANC_Cfg.GainMin);
//            printf(" ModeMax: %d\n", gc_Para_AdapANC_Cfg.ModeMax);
//            printf("---------------------------------\n");
        } else {
            bsp_anc_alg_dbg_tx_ack(ANC_ALG_DBG_ACK_ALG_PARAM_ERR);
            return;
        }

        alg_en = true;
    }
#endif // ANC_SNDP_SAE_SHIELD_ADAPTER_EN

    if (!alg_en) {
        bsp_anc_alg_dbg_tx_ack(ANC_ALG_DBG_ACK_ALG_TYPE_ERR);
    } else {
        bsp_anc_alg_dbg_tx_ack(ANC_ALG_DBG_ACK_OK);
    }

    memset(eq_rx_buf, 0, EQ_BUFFER_LEN);
}

#endif // ANC_ALG_DBG_EN

#endif // ANC_ALG_EN
