/*****************************************************************************
 * Copyright (c) 2023 Shenzhen Bluetrum Technology Co.,Ltd. All rights reserved.
 * File      : bsp_effect.h
 * Function  : 音乐音效模块接口
 * History   :
 * Created by yutao on 2023-12-1.
 *****************************************************************************/
#ifndef __BSP_EFFECT_H__
#define __BSP_EFFECT_H__

typedef enum {
    MUSIC_EFFECT_DBB            = 0,        //动态低音音效
    MUSIC_EFFECT_SPATIAL_AUDIO,             //空间音效
    MUSIC_EFFECT_VBASS,                     //虚拟低音音效
    MUSIC_EFFECT_DYEQ,                      //动态EQ音效
    MUSIC_EFFECT_XDRC,                      //2段DRC
    MUSIC_EFFECT_ALG_USER       = 8,        //用户自定义音效算法
    MUSIC_EFFECT_MAX            = 16,
} MUSIC_EFFECT_ALG;


void music_effect_init(void);
void music_effect_sco_audio_init_do(void);
void music_effect_sco_audio_exit_do(void);
void music_effect_sfunc_sleep_do(u8 enter);
void music_effect_func_process(void);
int music_effect_set_state(MUSIC_EFFECT_ALG alg, u8 state); //设置音效状态
bool music_effect_get_state(MUSIC_EFFECT_ALG alg);          //获取音效是否已经设置打开（不包含因通话、提示音等暂停的状态）
bool music_effect_get_state_real(MUSIC_EFFECT_ALG alg);     //获取音效实际的状态

///动态低音音效
void music_dbb_audio_start(void);
void music_dbb_audio_stop(void);
void music_dbb_audio_set_vol_do(u8 vol_level);
void music_dbb_audio_set_bass_level(u8 bass_level);

///空间音效
void music_spatial_audio_start(void);
void music_spatial_audio_stop(void);

///虚拟低音
void music_vbass_audio_start(void);
void music_vbass_audio_stop(void);
void music_vbass_set_param(u32 cutoff_frequency, u32 intensity, u8 vbass_high_frequency_set, u32 pregain);

///动态EQ
void music_dyeq_audio_start(void);
void music_dyeq_audio_stop(void);
void music_dyeq_audio_set_vol_do(u8 vol_level);
void music_dyeq_set_param(u8 *buf);
void music_dyeq_drc_set_param(void *buf);

///2段DRC
void music_xdrc_audio_start(void);
void music_xdrc_audio_stop(void);
void music_xdrc_audio_set_vol_do(u8 vol_level);
void music_xdrc_set_delay(u16 delay_samples);

///用户自定义音效算法
void music_effect_alg_user_start(void);
void music_effect_alg_user_stop(void);

#endif //__BSP_EFFECT_H__
