#include "include.h"

#define FS_CRC_SEED         0xffff

uint calc_crc(void *buf, uint len, uint seed);
bool mp3_res_play_kick(u32 addr, u32 len, bool kick);
bool wav_res_play_kick(u32 addr, u32 len, bool kick);
void wav_res_dec_process(void);
bool wav_res_is_play(void);
bool wav_res_stop(void);
void mp3_res_play_exit(void);

#if FUNC_MUSIC_EN

#if BT_BACKSTAGE_EN
const u32 *res_addr[] = {
#if WARNING_FUNC_MUSIC
    &RES_BUF_MUSIC_MODE_MP3,
#endif
#if WARNING_USB_SD
    &RES_BUF_SDCARD_MODE_MP3,
    &RES_BUF_USB_MODE_MP3,
#endif
#if WARNING_FUNC_BT
    &RES_BUF_BT_MODE_MP3,
#endif
#if WARNING_FUNC_FMRX
    &RES_BUF_FM_MODE_MP3,
#endif
#if WARNING_FUNC_AUX
    &RES_BUF_AUX_MODE_MP3,
#endif
};
#endif

//扫描全盘文件
bool pf_scan_music(u8 new_dev)
{
    if (new_dev) {
#if USB_SD_UPDATE_EN
        func_update();                                  //尝试升级
#endif // USB_SD_UPDATE_EN
    }

#if REC_FAST_PLAY
    f_msc.rec_scan = BIT(0);
    sys_cb.rec_num = 0;
    sys_cb.ftime = 0;
#endif // REC_FAST_PLAY

    f_msc.file_total = fs_get_total_files();
    if (!f_msc.file_total) {
        f_msc.dir_total = 0;
        return false;
    }

#if REC_FAST_PLAY
    f_msc.rec_scan = 0;
#endif // REC_FAST_PLAY

    if (new_dev) {
#if MUSIC_PLAYDEV_BOX_EN
        gui_box_show_playdev();
#endif // MUSIC_PLAYDEV_BOX_EN

#if WARNING_USB_SD
        if (sys_cb.cur_dev <= DEV_SDCARD1) {
            mp3_res_play(RES_BUF_SDCARD_MODE_MP3, RES_LEN_SDCARD_MODE_MP3);
        } else {
            mp3_res_play(RES_BUF_USB_MODE_MP3, RES_LEN_USB_MODE_MP3);
        }
#endif // WARNING_USB_SD
    }

    f_msc.dir_total = fs_get_dirs_count();          //获取文件夹总数
    return true;
}

#if MUSIC_PLAYMODE_NUM
//music播放模式切换
void music_playmode_next(void)
{
    sys_cb.play_mode++;
    if (sys_cb.play_mode >= MUSIC_PLAYMODE_NUM) {
        sys_cb.play_mode = NORMAL_MODE;
    }
    gui_box_show_playmode();
    plugin_playmode_warning();
}
#endif // MUSIC_PLAYMODE_NUM

#if MUSIC_BREAKPOINT_EN
void bsp_music_breakpoint_clr(void)
{
    f_msc.brkpt.file_ptr = 0;
    f_msc.brkpt.frame_count = 0;
    param_msc_breakpoint_write();
    param_sync();
    //printf("%s\n", __func__);
}

void bsp_music_breakpoint_init(void)
{
    int clr_flag = 0;
    if (f_msc.brkpt_flag) {
        f_msc.brkpt_flag = 0;
        param_msc_breakpoint_read();
        //printf("restore: %d, %d, %04x\n", f_msc.brkpt.file_ptr, f_msc.brkpt.frame_count, f_msc.brkpt.fname_crc);
        if (calc_crc(f_msc.fname, 8, FS_CRC_SEED) == f_msc.brkpt.fname_crc) {
            music_set_jump(&f_msc.brkpt);
        } else {
            clr_flag = 1;
        }
    }
    f_msc.brkpt.fname_crc = calc_crc(f_msc.fname, 8, FS_CRC_SEED);
    f_msc.brkpt.file_ptr = 0;
    f_msc.brkpt.frame_count = 0;
    if (clr_flag) {
        param_msc_breakpoint_write();
    }
}

void bsp_music_breakpoint_save(void)
{
    music_get_breakpiont(&f_msc.brkpt);
    param_msc_breakpoint_write();
    param_sync();
    //printf("save: %d, %d, %04x\n", f_msc.brkpt.file_ptr, f_msc.brkpt.frame_count, f_msc.brkpt.fname_crc);
}
#endif // MUSIC_BREAKPOINT_EN
#endif // FUNC_MUSIC_EN


void mp3_res_play(u32 addr, u32 len)
{
#if QTEST_EN
    if(qtest_get_mode()){
        return;
    }
#endif

    u16 msg;
    u8 mute_bak;
//    printf("%s: addr: %x, len: %x\n", __func__, addr, len);
    if (len == 0) {
        return;
    }

#if BT_BACKSTAGE_EN
    if (func_cb.sta_break != FUNC_NULL) {
        for (u32 i = 0; i < sizeof(res_addr) / sizeof(u32); i++) {
            if (*res_addr[i] == addr) {
                if (func_cb.sta != FUNC_BT) {
                    func_cb.sta_break = FUNC_NULL;
                }
                return;
            }
        }
    }
#endif

#if DAC_DNR_EN
    u8 sta = dac_dnr_get_sta();
    dac_dnr_set_sta(0);
#endif
    u8 dac_sta = dac_get_pwr_sta();
    func_bt_set_dac(1);

#if SYS_KARAOK_EN
    u8 voice_bak = 0, func_sta = func_cb.sta;
    if (karaok_get_voice_rm_sta()) {
        voice_bak = 1;
        karaok_voice_rm_disable();
    }
    bsp_karaok_exit(AUDIO_PATH_KARAOK);
#endif

    mute_bak = sys_cb.mute;
    if (sys_cb.mute) {
        bsp_loudspeaker_unmute();
    }
    if(get_music_dec_sta() != MUSIC_STOP) { //避免来电响铃/报号未完成，影响get_music_dec_sta()状态
        music_control(MUSIC_MSG_STOP);
    }
    bsp_change_volume(WARNING_VOLUME);

    mp3_res_play_kick(addr, len, true);

    while (get_music_dec_sta() != MUSIC_STOP) {
        bt_thread_check_trigger();
        sys_run_loop();
        WDT_CLR();
        msg = msg_dequeue();
        if (sys_cb.voice_evt_brk_en) {
            if (((msg == EVT_SD_INSERT) || (msg == EVT_UDISK_INSERT)) && (func_cb.sta != FUNC_MUSIC)) {
                func_message(msg);
                break;
            }
        }
#if LINEIN_DETECT_EN
        if ((msg == EVT_LINEIN_INSERT) && ((sys_cb.voice_evt_brk_en) || (LINEIN_2_PWRDOWN_EN))) {
            func_message(msg);
            break;
        }
#endif // LINEIN_DETECT_EN
        if (msg != NO_MSG) {
            msg_enqueue(msg);       //还原未处理的消息
        }
#if (CHARGE_BOX_TYPE == CBOX_SSW)
        if(bt_get_status() >= BT_STA_IDLE) {
            charge_box_ssw_process(0);
        }
#endif
#if BT_TWS_EN && WARNING_BREAK_EN
        if(sys_cb.tws_res_brk) {
            sys_cb.tws_res_brk = 0;
            break;
        }
#endif
    }
    music_control(MUSIC_MSG_STOP);
    bsp_change_volume(sys_cb.vol);
    mp3_res_play_exit();
    sys_cb.mute = mute_bak;
    if (sys_cb.mute) {
        bsp_loudspeaker_mute();
    }
    func_bt_set_dac(dac_sta);
#if DAC_DNR_EN
    dac_dnr_set_sta(sta);
#endif
#if SYS_KARAOK_EN
    if (voice_bak) {
        karaok_voice_rm_enable();
    }
    bsp_karaok_init(AUDIO_PATH_KARAOK, func_sta);
#endif
}

#if WARNING_SYSVOL_ADJ_EN
void bsp_res_sysvol_adjust(void)
{
    if (sys_cb.vol == VOL_MAX) {
        dac_vol_set(WARNING_SYSVOL_ADJ_LEVEL);
    }
}

void bsp_res_sysvol_resume(void)
{
    bsp_change_volume(sys_cb.vol);
}
#endif

void wav_res_play(u32 addr, u32 len)
{
    if (len == 0) {
        return;
    }

    u8 dac_sta = dac_get_pwr_sta();
    func_bt_set_dac(1);

#if DAC_DNR_EN
    u8 sta = dac_dnr_get_sta();
    dac_dnr_set_sta(0);
#endif

#if TINY_TRANSPARENCY_EN
    sys_cb.ttp_sta = sys_cb.ttp_start;
    bsp_ttp_stop();
#endif

#if WARNING_SYSVOL_ADJ_EN
    bsp_res_sysvol_adjust();
#endif

    sys_clk_req(INDEX_RES_PLAY, SYS_120M);

    wav_res_play_kick(addr, len, 1);
    while (wav_res_is_play()) {
        bt_thread_check_trigger();
        wav_res_dec_process();
        WDT_CLR();
#if BT_TWS_EN && WARNING_BREAK_EN
        if(sys_cb.tws_res_brk) {
            sys_cb.tws_res_brk = 0;
            break;
        }
#endif
    }

    wav_res_stop();
    sys_clk_free(INDEX_RES_PLAY);

#if WARNING_SYSVOL_ADJ_EN
    bsp_res_sysvol_resume();
#endif

#if DAC_DNR_EN
    dac_dnr_set_sta(sta);
#endif

    func_bt_set_dac(dac_sta);

#if TINY_TRANSPARENCY_EN
    if (sys_cb.ttp_sta) {
        bsp_ttp_start();
    }
#endif
}

void piano_res_play(u8 type, void *res)
{
    u8 dac_sta = dac_get_pwr_sta();
    func_bt_set_dac(1);

#if WARNING_SYSVOL_ADJ_EN
    bsp_res_sysvol_adjust();
#endif

    tone_play_kick(type, res, true);
    while (tone_is_playing()) {
        bt_thread_check_trigger();
        piano_play_process();
    }
    tone_play_end();

#if WARNING_SYSVOL_ADJ_EN
    bsp_res_sysvol_resume();
#endif

    func_bt_set_dac(dac_sta);
}

//提示音播报完毕，设置状态
void bsp_res_play_exit_cb(uint8_t res_idx)
{
#if BT_LOW_LATENCY_EN
    if (TWS_RES_MUSIC_MODE == res_idx) {
        printf("music mode\n");
        bt_low_latency_disable();
    } else if (TWS_RES_GAME_MODE == res_idx) {
        printf("game mode\n");
        bt_low_latency_enable();
    }
#endif

#if ANC_EN
    uint8_t anc_mode = 0;
    if (TWS_RES_NR_DISABLE == res_idx) {
        anc_mode = 1;
    } else if (TWS_RES_ANC == res_idx) {
        anc_mode = 2;
    } else if (TWS_RES_TRANSPARENCY == res_idx) {
        anc_mode = 3;
    }
    if (anc_mode) {
        bsp_anc_set_mode(anc_mode - 1);

        bsp_param_write(&anc_mode, PARAM_ANC_NR_STA, 1);
        bsp_param_sync();
    }
#endif
}

#if !BT_TWS_EN
uint8_t bsp_res_play(uint8_t res_idx)
{
    uint8_t res_type = tws_res_get_type(res_idx);

    if (res_type != RES_TYPE_INVALID) {
        u32 addr, len;

        tws_res_get_addr(res_idx, &addr, &len);
        if (len != 0) {
            if(res_type == RES_TYPE_MP3) {
                bt_audio_bypass();
                mp3_res_play(addr, len);
                bsp_res_play_exit_cb(res_idx);
                bt_audio_enable();
            } else if(res_type == RES_TYPE_WAV) {
                wav_res_play(addr, len);
            } else if(res_type == RES_TYPE_PIANO || res_type == RES_TYPE_TONE) {
                int type = (res_type == RES_TYPE_TONE)? WARNING_TONE : WARNING_PIANO;
                piano_res_play(type, (void *)addr);
            }
        }
    }

    return RES_ERR_INVALID;
}
#endif
